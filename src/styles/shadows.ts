import Common from "../utilities/Common";
import { colors } from ".";

export const BLUE_SHADOW = {
	shadowColor: "#14458D",
	shadowOffset: {
		width: 0,
		height: 4,
	},
	shadowOpacity: 0.10,
	shadowRadius: 10.00,
	elevation: 1,
};