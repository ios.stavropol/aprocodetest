import Common from "../utilities/Common";
import { colors } from ".";
// FONT FAMILY
export const FONT_FAMILY_REGULAR = 'OpenSans-Regular';
export const FONT_FAMILY_BOLD = 'OpenSans-Bold';
export const FONT_FAMILY_SEMIBOLD = 'OpenSans-SemiBold';
export const FONT_FAMILY_MEDIUM = 'OpenSans-Medium';

export const T_14_500 = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_MEDIUM,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(14),
};

export const T_16_500 = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_MEDIUM,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(16),
};

export const T_17_500 = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_MEDIUM,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(17),
};

export const T_13_400 = {
	color: colors.TEXT_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: 'normal',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(13),
};