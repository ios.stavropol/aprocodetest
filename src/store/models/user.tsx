//@ts-nocheck
import {createModel} from '@rematch/core';
import {API, StorageHelper} from './../../services';
import {ErrorsHelper} from './../../helpers';
import type {RootModel} from './../models';
import {BASE_URL} from './../../constants';
import Common from './../../utilities/Common';
import AsyncStorage from '@react-native-async-storage/async-storage';

type UserState = {
  isRequestGoing: false;
  subjects: [];
};

const user = createModel<RootModel>()({
  state: {
    isRequestGoing: false,
    subjects: [],
  } as UserState,
  reducers: {
    setRequestGoingStatus: (state, payload: boolean) => ({
      ...state,
      isRequestGoing: payload,
    }),
    setSubjects: (state, payload: object) => ({
      ...state,
      subjects: payload,
    }),
  },
  effects: dispatch => {
    return {
      async login(payload) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .login(payload)
            .then(response => {
              console.warn('login -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn(err.response);
              reject(err.response.data.message);
            });
        });
      },
    };
  },
});

export default user;
