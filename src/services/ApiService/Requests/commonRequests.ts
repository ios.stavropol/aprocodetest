import axios from 'axios';
import { BASE_URL } from './../../../constants';

class CommonAPI {
	login = (payload) => {
		let config = {
			method: 'get',
			url: BASE_URL + '/api/user/login',
			headers: { 'Authorization': 'Basic '+ payload}
		};
		return axios(config);
	};
}

const commonRequests = new CommonAPI();
export default commonRequests;
