import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { BASE_URL } from './../../constants';
import { commonRequests } from './Requests';

export interface ApiResponse<T> {
	status: number;
	data: T;
	points: T;
	message: string;
}

axios.interceptors.request.use(
	async (config) => {
		console.warn('Request', config.headers);
		const newConfig: AxiosRequestConfig = {
			...config,
			baseURL: BASE_URL,
			// withCredentials: true,
			// credentials: true,
			headers: { ...config.headers, 'Content-Type': 'application/json', 'Accept': 'application/json' },
		};

		return newConfig;
	},
	(error: AxiosError) => {
		return Promise.reject(error);
	},
);

class APIService {
	common = commonRequests;

	setToken = (token: string) => {
		console.warn('APIService -> setToken -> token', token);
		axios.defaults.withCredentials = true;
	};

	loadToken = (token: string) => {
		console.warn('APIService -> loadToken -> token', token);
		axios.defaults.headers['Cookie'] = 'PHPSESSID=' + token + '; path=/; domain=' + BASE_URL + '; HttpOnly';
	};
}

const API = new APIService();
export default API;
