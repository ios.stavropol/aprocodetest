import React, {useCallback, useEffect, useRef, useState} from 'react';
import {View, Text, TouchableOpacity, ScrollView} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import FilterView from '../../components/Main/FilterView';
import ItemView from '../../components/Main/ItemView';
import PromptModalView from '../../components/Main/PromptModalView';
import prompt from 'react-native-prompt-android';

const MainScreen = ({setSubjects, subjects}) => {
  const navigation = useNavigation();

  const [showPrompt, setShowPrompt] = React.useState(false);
  const [body, setBody] = React.useState(null);
  const [index, setIndex] = React.useState(0);

  useEffect(() => {
    let newArray = JSON.parse(JSON.stringify(subjects));
    let newSubjects = [];
    if (index === 1) {
      for (let i = 0; i < newArray?.length; i++) {
        if (newArray[i].selected) {
          newSubjects.push(newArray[i]);
        }
      }
    } else if (index === 2) {
      for (let i = 0; i < newArray?.length; i++) {
        if (!newArray[i].selected) {
          newSubjects.push(newArray[i]);
        }
      }
    } else {
      newSubjects = JSON.parse(JSON.stringify(subjects));
    }
    let array = [];
    for (let i = 0; i < newSubjects?.length; i++) {
      array.push(
        <ItemView
          data={newSubjects[i]}
          last={i + 1 === newSubjects?.length ? true : false}
          style={{
            marginTop: i === 0 ? Common.getLengthByIPhone7(16) : 0,
          }}
          onDelete={item => {
            let objs = JSON.parse(JSON.stringify(subjects));
            for (let y = 0; y < objs?.length; y++) {
              if (objs[y].id === item.id) {
                objs.splice(y, 1);
                break;
              }
            }
            setSubjects(objs);
          }}
          onClick={item => {
            let objs = JSON.parse(JSON.stringify(subjects));
            for (let y = 0; y < objs?.length; y++) {
              if (objs[y].id === item.id) {
                objs[y].selected = !objs[y].selected;
                break;
              }
            }
            setSubjects(objs);
          }}
        />,
      );
    }
    setBody(array);
  }, [subjects, index]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <FilterView
        onFilter={ind => {
          setIndex(ind);
        }}
      />
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        {body}
        <TouchableOpacity
          style={{
            marginTop: Common.getLengthByIPhone7(25),
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
            height: Common.getLengthByIPhone7(55),
            borderRadius: Common.getLengthByIPhone7(8),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: colors.BLUE_COLOR,
          }}
          onPress={() => {
            setShowPrompt(true);
          }}>
          <Text
            style={[
              typography.T_16_500,
              {
                color: 'white',
              },
            ]}
            numberOfLines={1}
            allowFontScaling={false}>
            Добавить
          </Text>
        </TouchableOpacity>
      </ScrollView>
      <PromptModalView
        show={showPrompt}
        onClose={() => {
          setShowPrompt(false);
        }}
        onSave={obj => {
          let array = JSON.parse(JSON.stringify(subjects));
          array.push(obj);
          setSubjects(array);
          setShowPrompt(false);
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({
  subjects: state.user.subjects,
});

const mdtp = (dispatch: Dispatch) => ({
  setSubjects: payload => dispatch.user.setSubjects(payload),
});

export default connect(mstp, mdtp)(MainScreen);
