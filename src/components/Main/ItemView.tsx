import React, {useCallback, useEffect, useRef, useState} from 'react';
import {Platform, View, Text, TouchableOpacity, Image} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';

const ItemView = ({data, style, last, onDelete, onClick}) => {
  const [loading, setLoading] = React.useState(false);

  useEffect(() => {}, []);

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
          paddingTop: Common.getLengthByIPhone7(10),
          paddingBottom: Common.getLengthByIPhone7(10),
          borderBottomColor: '#eef8fd',
          borderBottomWidth: last ? 0 : 1,
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row',
        },
        style,
      ]}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{
            width: Common.getLengthByIPhone7(30),
            height: Common.getLengthByIPhone7(30),
            borderRadius: Common.getLengthByIPhone7(8),
            borderWidth: data?.selected ? 0 : 1,
            borderColor: colors.BORDER_COLOR,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: data?.selected ? colors.GREEN_COLOR : 'white',
          }}
          activeOpacity={1}
          onPress={() => {
            if (onClick) {
              onClick(data);
            }
          }}>
          <Image
            source={require('./../../assets/ic-tick.png')}
            style={{
              width: Common.getLengthByIPhone7(13),
              height: Common.getLengthByIPhone7(9),
              resizeMode: 'contain',
              tintColor: data?.selected ? 'white' : null,
            }}
          />
        </TouchableOpacity>
        <View
          style={{
            marginLeft: Common.getLengthByIPhone7(20),
          }}>
          <Text
            style={[
              typography.T_17_500,
              {
                width: Common.getLengthByIPhone7(230),
              },
            ]}
            allowFontScaling={false}>
            {data?.title}
          </Text>
          <Text
            style={[
              typography.T_13_400,
              {
                width: Common.getLengthByIPhone7(230),
                color: colors.TEXT_GRAY_COLOR,
                textDecorationLine: data?.selected ? 'line-through' : null,
              },
            ]}
            allowFontScaling={false}>
            {data?.task}
          </Text>
        </View>
      </View>
      <TouchableOpacity
        style={{
          width: Common.getLengthByIPhone7(40),
          height: Common.getLengthByIPhone7(40),
          borderRadius: Common.getLengthByIPhone7(8),
          backgroundColor: colors.VIEW_COLOR,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onPress={() => {
          if (onDelete) {
            onDelete(data);
          }
        }}>
        <Image
          source={require('./../../assets/ic-trash.png')}
          style={{
            width: Common.getLengthByIPhone7(24),
            height: Common.getLengthByIPhone7(24),
            resizeMode: 'contain',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  getImportant: payload => dispatch.user.getImportant(payload),
});

export default connect(mstp, mdtp)(ItemView);
