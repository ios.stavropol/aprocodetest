import React, {useEffect} from 'react';
import {Alert, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography} from './../../styles';
import Modal from 'react-native-modal';
import {APP_NAME} from '../../constants';

const FilterModalView = ({selected, show, onSelect}) => {
  const [title, setTitle] = React.useState('');
  const [task, setTask] = React.useState('');

  const renderRow = (index, title, style) => {
    return (
      <TouchableOpacity
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(16),
            height: Common.getLengthByIPhone7(56),
            alignItems: 'center',
            justifyContent: 'center',
            borderBottomColor: 'white',
            borderBottomWidth: 1,
          },
          style,
        ]}
        onPress={() => {
          if (onSelect) {
            onSelect(index);
          }
        }}>
        <Text
          style={[
            typography.T_17_500,
            {
              color: index === selected ? colors.BLUE_COLOR : '#737A82',
            },
          ]}
          numberOfLines={1}
          allowFontScaling={false}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <Modal
      isVisible={show}
      backdropColor={colors.MAIN_COLOR}
      backdropOpacity={0.54}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(16),
          borderRadius: Common.getLengthByIPhone7(14),
          backgroundColor: 'rgba(249, 249, 249, 0.94)',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {renderRow(0, 'Показывать все задания', {})}
        {renderRow(1, 'Выполненные', {})}
        {renderRow(2, 'Не выполненные', {borderBottomWidth: 0})}
      </View>
    </Modal>
  );
};

const mstp = (state: RootState) => ({
  showMenu: state.buttons.showMenu,
});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(FilterModalView);
