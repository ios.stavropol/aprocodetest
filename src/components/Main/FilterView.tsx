import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Platform,
  View,
  Text,
  TouchableOpacity,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import FilterModalView from './FilterModalView';

const titles = ['Показывать все задания', 'Выполненные', 'Не выполненные'];

const FilterView = ({onFilter}) => {
  const [showFilter, setShowFilter] = React.useState(false);
  const [index, setIndex] = React.useState(0);

  useEffect(() => {}, []);

  return (
    <View
      style={{
        width: Common.getLengthByIPhone7(0),
        height: Common.getLengthByIPhone7(127),
        borderBottomColor: '#eef8fd',
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <TouchableOpacity
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(32),
          height: Common.getLengthByIPhone7(36),
          borderRadius: Common.getLengthByIPhone7(8),
          borderWidth: 2,
          borderColor: colors.BLUE_COLOR,
          alignItems: 'center',
          justifyContent: 'center',
        }}
        onPress={() => {
          setShowFilter(true);
        }}>
        <Text
          style={[
            typography.T_14_500,
            {
              color: colors.BLUE_COLOR,
            },
          ]}
          numberOfLines={1}
          allowFontScaling={false}>
          {titles[index]}
        </Text>
      </TouchableOpacity>
      <FilterModalView
        show={showFilter}
        selected={index}
        onSelect={ind => {
          setShowFilter(false);
          setIndex(ind);
          if (onFilter) {
            onFilter(ind);
          }
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  getImportant: payload => dispatch.user.getImportant(payload),
});

export default connect(mstp, mdtp)(FilterView);
