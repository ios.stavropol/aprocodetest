import React, {useEffect} from 'react';
import {Alert, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography} from './../../styles';
import Modal from 'react-native-modal';
import {APP_NAME} from '../../constants';

const PromptModalView = ({show, onClose, onSave}) => {
  const [title, setTitle] = React.useState('');
  const [task, setTask] = React.useState('');

  return (
    <Modal
      isVisible={show}
      backdropColor={colors.MAIN_COLOR}
      backdropOpacity={0.54}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          width: Common.getLengthByIPhone7(270),
          borderRadius: Common.getLengthByIPhone7(14),
          backgroundColor: 'rgba(249, 249, 249, 0.94)',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(270),
            padding: Common.getLengthByIPhone7(16),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={[typography.T_17_500, {}]}
            numberOfLines={1}
            allowFontScaling={false}>
            Добавить предмет
          </Text>
          <Text
            style={[
              typography.T_13_400,
              {
                color: colors.TEXT_GRAY_COLOR,
                marginTop: 4,
              },
            ]}
            numberOfLines={1}
            allowFontScaling={false}>
            Укажите заголовок и задание
          </Text>
          <TextInput
            style={[
              typography.T_13_400,
              {
                width:
                  Common.getLengthByIPhone7(270) -
                  Common.getLengthByIPhone7(32),
                height: Common.getLengthByIPhone7(32),
                borderRadius: Common.getLengthByIPhone7(8),
                borderColor: 'rgba(60, 60, 67, 0.3)',
                borderWidth: 1,
                backgroundColor: 'white',
                color: colors.TEXT_COLOR,
                textAlign: 'left',
                paddingLeft: Common.getLengthByIPhone7(6),
                paddingRight: Common.getLengthByIPhone7(6),
                marginTop: Common.getLengthByIPhone7(16),
              },
            ]}
            placeholder={'Заголовок'}
            allowFontScaling={false}
            underlineColorAndroid={'transparent'}
            onSubmitEditing={() => {}}
            onFocus={() => {}}
            onBlur={() => {}}
            onChangeText={code => {
              setTitle(code);
            }}
            value={title}
          />
          <TextInput
            style={[
              typography.T_13_400,
              {
                width:
                  Common.getLengthByIPhone7(270) -
                  Common.getLengthByIPhone7(32),
                height: Common.getLengthByIPhone7(32),
                borderRadius: Common.getLengthByIPhone7(8),
                borderColor: 'rgba(60, 60, 67, 0.3)',
                borderWidth: 1,
                backgroundColor: 'white',
                color: colors.TEXT_COLOR,
                textAlign: 'left',
                paddingLeft: Common.getLengthByIPhone7(6),
                paddingRight: Common.getLengthByIPhone7(6),
                marginTop: Common.getLengthByIPhone7(16),
              },
            ]}
            placeholder={'Задание'}
            allowFontScaling={false}
            underlineColorAndroid={'transparent'}
            onSubmitEditing={() => {}}
            onFocus={() => {}}
            onBlur={() => {}}
            onChangeText={code => {
              setTask(code);
            }}
            value={task}
          />
        </View>
        <View
          style={{
            width: Common.getLengthByIPhone7(270),
            height: Common.getLengthByIPhone7(44),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderTopColor: 'white',
            borderTopWidth: 1,
          }}>
          <TouchableOpacity
            style={{
              width: Common.getLengthByIPhone7(270) / 2,
              height: Common.getLengthByIPhone7(44),
              alignItems: 'center',
              justifyContent: 'center',
              borderRightColor: 'white',
              borderRightWidth: 1,
            }}
            onPress={() => {
              if (onClose) {
                onClose();
              }
            }}>
            <Text
              style={[
                typography.T_17_500,
                {
                  color: colors.TEXT_GRAY_COLOR,
                },
              ]}
              numberOfLines={1}
              allowFontScaling={false}>
              Отмена
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: Common.getLengthByIPhone7(270) / 2,
              height: Common.getLengthByIPhone7(44),
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => {
              if (title?.length && task?.length) {
                if (onSave) {
                  onSave({
                    id: new Date().getTime(),
                    title,
                    task,
                    selected: false,
                  });
                  setTitle('');
                  setTask('');
                }
              } else {
                Alert.alert(APP_NAME, 'Заполните все поля!');
              }
            }}>
            <Text
              style={[
                typography.T_17_500,
                {
                  color: colors.BLUE_COLOR,
                },
              ]}
              numberOfLines={1}
              allowFontScaling={false}>
              Сохранить
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const mstp = (state: RootState) => ({
  showMenu: state.buttons.showMenu,
});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(PromptModalView);
