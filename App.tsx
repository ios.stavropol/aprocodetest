import React, {useEffect} from 'react';
import Router from './src/navigation/Router';
import {StatusBar, Platform} from 'react-native';
import useAppState from 'react-native-appstate-hook';
import {Provider} from 'react-redux';
import {theme} from './theme';
import {ThemeProvider} from 'styled-components';
import store from './src/store';
import {colors} from './src/styles';

const App = () => {
  useEffect(() => {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('dark-content', true);
    } else {
      StatusBar.setBarStyle('dark-content', true);
      StatusBar.setBackgroundColor('white', true);
    }
  }, []);

  useAppState({
    onChange: newAppState => {},
    onForeground: () => {},
    onBackground: () => console.warn('App went to background'),
  });

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Router />
      </ThemeProvider>
    </Provider>
  );
};

export default App;
